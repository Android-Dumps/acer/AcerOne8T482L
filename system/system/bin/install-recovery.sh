#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/platform/bootdevice/by-name/recovery:33554432:6345cf391b89cd62edf0897b5f7a436a27838bbc; then
  applypatch  \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/platform/bootdevice/by-name/boot:33554432:03feb431f897224b202f3c1be61f2c9cb2186cfa \
          --target EMMC:/dev/block/platform/bootdevice/by-name/recovery:33554432:6345cf391b89cd62edf0897b5f7a436a27838bbc && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
